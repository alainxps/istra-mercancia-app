var app = angular.module("istra-mercancia", ["ngRoute"]);

app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "views/login.html"
    })
    .when("/registro/datos", {
        templateUrl : "views/registro-datos.html"
    })
    .when("/registro/localizacion", {
        templateUrl : "views/registro-localizacion.html"
    })
    .when("/registro/representante", {
        templateUrl : "views/registro-representante.html"
    })
    .when("/inicio", {
        templateUrl : "views/inicio.html"
    })
    .when("/razones", {
        templateUrl : "views/razones.html"
    })
    .when("/razones/datos", {
        templateUrl : "views/razones-datos.html"
    })
    .when("/razones/localizacion", {
        templateUrl : "views/razones-localizacion.html"
    })
    .when("/razones/control140", {
        templateUrl : "views/razones-control140.html"
    });
});